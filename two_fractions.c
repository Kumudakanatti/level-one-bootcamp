#include<stdio.h>
typedef struct fraction
  {
    int num;
    int deno;
  }Fraction;
  Fraction input()
   {
      Fraction x;
      printf("Enter the numerator \n");
      scanf("%d",&x.num);
      printf("Enter the denominator \n");
      scanf("%d",&x.deno);
      printf("The entered fraction is: %d/%d\n",x.num,x.deno);
      return x;
    }
     int hcf(Fraction r)
       {
          int i,gcd;
          for(i=1;i<=r.num && i<=r.deno;i++)
             {
                 if(r.num%i==0 && r.deno%i==0)
                 gcd=i;
              }
              return gcd;
         }
        Fraction sum(Fraction x1,Fraction x2)
          {
             int gcd;
             Fraction temp;
             Fraction r;
             temp.num=((x1.num*x2.deno)+(x2.num*x1.deno));
             temp.deno=(x1.deno*x2.deno);
              gcd=hcf(temp);
              r.num=temp.num/gcd;
              r.deno=temp.deno/gcd;
              return r;
           }
           void result(Fraction x1, Fraction x2,Fraction r)
              {
      printf("The sum of %d%d + %d%d is %d/%d",x1.num,x1.deno,x2.num,x2.deno,r.num,r.deno);
              }
               int main()
             {
               Fraction x1,x2,res;
                printf("Enter the numerator and denominator of the first fraction:");
                x1=input();
                printf("Enter the numerator and denominator of the second fraction:");
                x2=input();
                res=sum(x1,x2);
                result(x1,x2,res);
                return 0;
             }
