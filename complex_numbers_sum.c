HSPC Problem
gradebook:
#include<stdio.h>
typedef struct
{
	char name[30];
	int marks[50];
	float average;
	char grade;
 }Student;
typedef struct
{
	int no_of_students;
	char course_name;
	int no_of_scores;
	float weights[50];
	Student s[50];
}gradebook;
int get no_of_gradebooks()
{
int n;
printf("Enter the number of gradebooks:\n");
scanf("%d",&n);
return n;
}
Student read_one_student(int m)
{
	Student s;
	printf("Enter the student name: \n");
	scanf("%s",&s.name);
	printf("Enter the student scores: \n");
	for(int i=0;i<m;i++)
	scanf("%d",&s.marks[i]);
	return s;
}
void read_n_students(int n,int m,Student s[n])
{
	for(int i=0;i<n;i++)
	s[i]=read_one_student(m);
}
gradebook read_one_gradebook()
{
	gradebook g;
	printf("\n Enter the course name: \n");
	scanf("%s",&g.course_name):
	printf("Enter the number of students: \n");
	scanf("%d",&g.no_of_students);
	printf("Enter the number of scores: \n");
	scanf("%d",&g.no_of_scores);
	printf(Enter the weights: \n");
	for(int i=0;i<g.no_of_scores;i++)
	scanf("%d",&g.weights[i]);
read_n_students(g.no_of_students,g.no_of_scores,g.s);
	return g;
}
void read_n_gradebook(int n, gradebook g[n])
{
	for(int i=0;i<n;i++)
	g[i]=read_one_gradebook();
}
char compute_one_grade(float avg)
{
	char grade;
	if(avg>=90 && avg<100)
	grade='A';
	else if(avg>=80 && avg<90)
	grade='B';
	else if(avg>=70 && avg<80)
	grade='C';
	else if(avg>=60 && avg<70)
	grade='D';
	else if(avg>=0 && avg <60)
	grade='F';
	return grade;
}
float compute_one_average(int m,float w[m],int marks[m])
{
	float average=0; float sum_of_w=0;
	for(int i=0;i<m;i++)
	{	
		average+=(marks[i]*w[i]);
		sum_of_w = sum_of_w+w[i];
	}
	average=average/sum_of_w;
}
void compute_n_student(int  n,int m,float w[m],Student s[n])
{
	for(int i=0;i<n;i++)
	{
s[i].average= compute_one_grade(s[i].average);
	}
}
void compute_n_gradebook(int n,gradebook g[n])
{
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<g[i].no_of_students;j++)
compute_n_student(g[i].no_of_students,g[i].no_of_scores,g[i].weights,g[i].s);
	}
}
void print_one_gradebook(gradebook g)
{
	printf("\n Course Name: %s\n",g.course_name);
	for(int i=0;i<g.no_of_students;i++)
		{
		printf("%s \ t%.2f %c\n",g.s[i].name, g.s[i].average, g.s[i].grade);
		}
}
void print_n_gradebook(int n,gradebook g[n])
{
	for(int i=0;i<n;i++)
		print_one_gradebook(g[i]);
}
int main()
{
	int n=get_no_of_gradebooks();
	gradebook g[n];
	read_n_gradebook(n,g);
	compute_n_gradebook(n,g);
	print_n_gradebook(n,g);
	return 0;
}
